﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyManager : MonoBehaviour
{

    InputField userNameInput;

    public void OnLogin()
    {
        if (string.IsNullOrEmpty(userNameInput.text))
            return;

        ConnectToLobby(userNameInput.text);
    }

    void ConnectToLobby(string _username)
    {

    }
}
