﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    static public GameManager Instance;

    public Cell[,] mCells;
    public List<Cell> mBarriers = new List<Cell>();

    public int width;
    public int height;

    public GameObject cellPrefab;
    public GameObject bombPrefab;

    public Sprite bombStrSprite, bombCountSprite, speedSpite;

	void Awake ()
    {
        Instance = this;
        mCells = new Cell[width + 2, height + 2];

        for (int x = 0; x < width + 2; x++)
        {
            for (int y = 0; y < height + 2; y++)
            {
                GameObject newCell = Instantiate(cellPrefab, transform);

                //<------------------------------------------------------ Создание стен
                if (y == 0 || y == height + 1 || x == 0 || x == width + 1)
                {
                    newCell.name = "WALL(" + x + "," + y + ")";

                    newCell.GetComponent<Cell>().Init(enumCellType.WALL, new Vector2(x,y));
                }
                //---------------------------------------------------------------------->
                else
                {
                    //<------------- Создание поля (обычных клеток и разрушаемых клеток)

                    // Рандомная генерация
                    //int i = Random.Range(0, 10);
                    //if (i != 0)

                    // нечетный Х или У
                    if (x % 2 != 0 || y % 2 != 0)
                    {
                        newCell.name = "(" + x + "," + y + ")";

                        // down-left
                        if (y + x < 5)
                            newCell.GetComponent<Cell>().Init(enumCellType.NORMAL, new Vector2(x, y));
                        // down-right
                        else if((x > width - 3 && y == 1) || (x == width && y < 4))
                            newCell.GetComponent<Cell>().Init(enumCellType.NORMAL, new Vector2(x, y));
                        // up-right
                        else if((x > width - 3 && y == width) || (x == width && y > height - 3))
                            newCell.GetComponent<Cell>().Init(enumCellType.NORMAL, new Vector2(x, y));
                        // up-left
                        else if((x == 1 && y > height - 3) || (y == height && x < 4))
                            newCell.GetComponent<Cell>().Init(enumCellType.NORMAL, new Vector2(x, y));
                        else
                        {
                            int i = /*Random.Range(0, 10)*/1;
                            if (i == 0)
                            {
                                newCell.GetComponent<Cell>().Init(enumCellType.NORMAL, new Vector2(x, y));
                            }
                            else
                            {
                                //BARRIER
                                newCell.GetComponent<Cell>().Init(enumCellType.BARRIER, new Vector2(x, y));
                                mBarriers.Add(newCell.GetComponent<Cell>());
                            }
                        }  
                    }
                    else
                    {
                        newCell.name = "WALL(" + x + "," + y + ")";
                        //WALL
                        newCell.GetComponent<Cell>().Init(enumCellType.WALL, new Vector2(x, y));
                    }
                    //---------------------------------------------------------------------->
                }
                mCells[x, y] = newCell.GetComponent<Cell>();
                newCell.transform.position = new Vector2(x,y);
            }
        }
	}

    public List<Cell> GetNeighbours(Cell _cell)
    {
        List<Cell> neighbours = new List<Cell>();


        if ((int)_cell.mPos.x + 1 <= width)
            neighbours.Add(mCells[(int)_cell.mPos.x + 1, (int)_cell.mPos.y]);
        if ((int)_cell.mPos.x - 1 >= 0)              
            neighbours.Add(mCells[(int)_cell.mPos.x - 1, (int)_cell.mPos.y]);
        if ((int)_cell.mPos.y + 1 <= height)              
            neighbours.Add(mCells[(int)_cell.mPos.x, (int)_cell.mPos.y + 1]);
        if ((int)_cell.mPos.x - 1 >= 0)
            neighbours.Add(mCells[(int)_cell.mPos.x, (int)_cell.mPos.y - 1]);

        return neighbours;
    }
}
