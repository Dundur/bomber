﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MainController
{
    Cell targetCell;
    bool isMooving = false;

    protected new void Start()
    {
        base.Start();
        //hitPoints = 100;

        //FindPath(GameManager.Instance.mCells[posX, posY], GameManager.Instance.mCells[PlayerController.Instance.posX, PlayerController.Instance.posY]);
    }

    override protected void Update()
    {
        if (!gm || hitPoints < 1)
            return;

        base.Update();

        #region TEMPPLAYERCONTROL
        //float h = Input.GetAxis("Horizontal") * (moveSpeed / 50);
        //float v = Input.GetAxis("Vertical") * (moveSpeed / 50);

        //CalculateAngle(h, v);
        //if (h > 0)
        //    if (gm.mCells[Mathf.RoundToInt(transform.position.x + 0.5f), Mathf.RoundToInt(transform.position.y)].mType != enumCellType.NORMAL)
        //        h = 0;
        //if (h < 0)
        //    if (gm.mCells[Mathf.RoundToInt(transform.position.x - 0.5f), Mathf.RoundToInt(transform.position.y)].mType != enumCellType.NORMAL)
        //        h = 0;
        //if (v > 0)
        //    if (gm.mCells[Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y + 0.5f)].mType != enumCellType.NORMAL)
        //        v = 0;
        //if (v < 0)
        //    if (gm.mCells[Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y - 0.5f)].mType != enumCellType.NORMAL)
        //        v = 0;

        //transform.position += new Vector3(h, v, 0);


        //if (Input.GetKeyUp("space") && bombsPlaced < bombCount)
        //{
        //    StartCoroutine(StartBombTimer(posX, posY));
        //}


        #endregion

        if (!isMooving)
        FindPath(GameManager.Instance.mCells[posX, posY],
            /*GameManager.Instance.mCells[PlayerController.Instance.posX, PlayerController.Instance.posY]*/
            GetNearestTarget()
            );

        // AI navigating system
        //if (!isMooving)
        //{
        //    foreach (var cell in gm.mCells)
        //    {
        //        if ((cell.willExplode || cell.mBuffType == enumBuffType.EXPLOSION) && gm.mCells[posX, posY] == cell)
        //        {
        //            if (EscapeExplosion())
        //            {
        //                StartCoroutine(LerpToPos(EscapeExplosion().mPos));
        //                break;
        //            }
        //        }
        //    }

        //    if (GetNearestBarrier() && bombsPlaced < bombCount)
        //        StartCoroutine(LerpToPos(GetNearestBarrier().mPos));
        //    else
        //        if(bombsPlaced < bombCount)
        //        StartCoroutine(StartBombTimer(posX, posY));
        //}


    }

    Cell GetNearestTarget()
    {
        float dist = Vector2.Distance(transform.position, PlayerController.Instance.transform.position);
        Cell tempCell = null/*, tempCell2 = null*/;

        // Explosion escape
        if (gm.mCells[posX,posY].willExplode)
        {
            for (int i = 0; i < gm.width; i++)
            {
                // right
                if (!thisCell(i, 0).willExplode && thisCell(i, 0).mType == enumCellType.NORMAL)
                    return thisCell(i, 0);
                else
                {
                    foreach (var c in gm.GetNeighbours(thisCell(i, 0)))
                        if (!c.willExplode && c.mType == enumCellType.NORMAL)
                        {
                            return c;
                        }
                }

                // left
                if (!thisCell(-i, 0).willExplode && thisCell(-i, 0).mType == enumCellType.NORMAL)
                    return thisCell(-i, 0);
                else
                {
                    foreach (var c in gm.GetNeighbours(thisCell(-i, 0)))
                        if (!c.willExplode && c.mType == enumCellType.NORMAL)
                        {
                            return c;
                        }
                }
            }

            for (int i = 0; i < gm.height; i++)
            {
                // top
                if (!thisCell(0, i).willExplode && thisCell(0, i).mType == enumCellType.NORMAL)
                    return thisCell(0, i);
                else
                {
                    foreach (var c in gm.GetNeighbours(thisCell(0, i)))
                        if (!c.willExplode && c.mType == enumCellType.NORMAL)
                        {
                            return c;
                        }
                }

                // bot
                if (!thisCell(0, -i).willExplode && thisCell(0, -i).mType == enumCellType.NORMAL)
                    return thisCell(0, -i);
                else
                {
                    foreach (var c in gm.GetNeighbours(thisCell(0, -i)))
                        if (!c.willExplode && c.mType == enumCellType.NORMAL)
                        {
                            return c;
                        }
                }
            }

        }

        // Normal behaviour
        foreach (var cell in gm.mBarriers)
        {
            if (Vector2.Distance(mPos(), cell.mPos) < dist)
            {
                dist = Vector2.Distance(mPos(), cell.mPos);
                tempCell = cell;
            }
        }

        if (tempCell)
        {
            foreach (var c in gm.GetNeighbours(tempCell))
            {
                if (c.mType == enumCellType.NORMAL)
                    return c;
            }
        }

        return gm.mCells[PlayerController.Instance.posX,PlayerController.Instance.posY];
    }

    void FindPath(Cell _startCell, Cell _endCell)
    {
        Debug.Log(_endCell.name);
        if (_endCell == null)
            return;
        foreach (var c in gm.mCells)
        {
            c.testText.text = "";
            //if (c.mType != enumCellType.WALL && c.GetComponent<SpriteRenderer>().color != Color.yellow)
            //    c.GetComponent<SpriteRenderer>().color = Color.gray;
        }

        _endCell.GetComponent<SpriteRenderer>().color = Color.red;

        List<Cell> openSet = new List<Cell>();
        HashSet<Cell> closedSet = new HashSet<Cell>();
        openSet.Add(_startCell);

        while (openSet.Count > 0)
        {
            Cell currentCell = openSet[0];
            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < currentCell.fCost || openSet[i].fCost == currentCell.fCost)
                {
                    if (openSet[i].hCost < currentCell.hCost)
                        currentCell = openSet[i];
                }
            }

            openSet.Remove(currentCell);
            closedSet.Add(currentCell);

            if (currentCell == _endCell)
            {
                RetracePath(_startCell, _endCell);
                return;
                //RetracePath(_startCell, _endCell);
                //return;
            }

            foreach (Cell neighbour in GameManager.Instance.GetNeighbours(currentCell))
            {
                if (neighbour.mType != enumCellType.NORMAL || closedSet.Contains(neighbour))
                    continue;

                int newMovementCostToNeighbour = currentCell.gCost + GetDistance(currentCell, neighbour);
                if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                {
                    neighbour.gCost = newMovementCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, _endCell);
                    neighbour.parent = currentCell;

                    if (!openSet.Contains(neighbour))
                    {
                        openSet.Add(neighbour);
                    }
                }
            }
        }
    }

    void RetracePath(Cell startCell, Cell endCell)
    {
        List<Cell> path = new List<Cell>();
        Cell currentCell = endCell;
        int i = 0;
        while (currentCell != startCell)
        {
            //yield return new WaitForSeconds(0.25f);

            path.Add(currentCell);
            currentCell = currentCell.parent;

            i++;
            if (i > 50)
                break;
        }

        path.Reverse();


        if (path.Count > 0)
        {
            if (thisCell().willExplode)
            {
                if (path[0].mType == enumCellType.NORMAL)
                    StartCoroutine(LerpToPos(path[0].mPos));
            }
            else
            {
                if (path[0].mType == enumCellType.NORMAL && !path[0].willExplode)
                    StartCoroutine(LerpToPos(path[0].mPos));
            }
        }
        else
        {
            if (bombsPlaced < bombCount)
            {
                StartCoroutine(StartBombTimer(posX, posY));
            }
        }


        for (int j = 0; j < path.Count; j++)
            path[j].testText.text = j.ToString();
        //path[j].GetComponent<SpriteRenderer>().color = Color.red;
    }


    int GetDistance(Cell _cellA, Cell _cellB)
    {
        int dstX = Mathf.Abs(_cellA.posX - _cellB.posX);
        int dstY = Mathf.Abs(_cellA.posY - _cellB.posY);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        return 14 * dstX + 10 * (dstY - dstX);
    }

    //Cell GetNearestBarrier()
    //{
    //    Cell tempCell = null/*, tempCell2 = null*/;
    //    float dist = Vector2.Distance(mPos(), PlayerController.Instance.mPos());

    //    foreach (var cell in gm.mBarriers)
    //    {
    //        if (Vector2.Distance(mPos(), cell.mPos) < dist)
    //        {
    //            dist = Vector2.Distance(mPos(), cell.mPos);
    //            tempCell = cell;
    //        }
    //    }

    //    // right
    //    if (tempCell.mPos.x > posX && gm.mCells[posX + 1, posY].mType == enumCellType.NORMAL)
    //    {
    //        targetCell = gm.mCells[posX + 1, posY];
    //        return targetCell;
    //    }
    //    // left
    //    else if (tempCell.mPos.x < posX && gm.mCells[posX - 1, posY].mType == enumCellType.NORMAL)
    //    {
    //        targetCell = gm.mCells[posX - 1, posY];
    //        return targetCell;
    //    }
    //    // up
    //    else if (tempCell.mPos.y > posY && gm.mCells[posX, posY + 1].mType == enumCellType.NORMAL)
    //    {
    //        targetCell = gm.mCells[posX, posY + 1];
    //        return targetCell;
    //    }
    //    // down
    //    else if (tempCell.mPos.y < posY && gm.mCells[posX, posY - 1].mType == enumCellType.NORMAL)
    //    {
    //        targetCell = gm.mCells[posX, posY - 1];
    //        return targetCell;
    //    }

    //    return null;
    //}

    //Cell EscapeExplosion()
    //{
    //    Cell tempCell = null;
    //    float dist = Vector2.Distance(mPos(), PlayerController.Instance.mPos());

    //    foreach (var cell in gm.mCells)
    //    {
    //        if (cell.mType == enumCellType.NORMAL && !cell.willExplode)
    //        {
    //            if (Vector2.Distance(mPos(), cell.mPos) < dist)
    //            {
    //                dist = Vector2.Distance(mPos(), cell.mPos);
    //                tempCell = cell;
    //            }
    //        }
    //    }

    //    // right
    //    if (tempCell.mPos.x > posX && gm.mCells[posX + 1, posY].mType == enumCellType.NORMAL && gm.mCells[posX + 1, posY].mBuffType != enumBuffType.EXPLOSION)
    //    {
    //        targetCell = gm.mCells[posX + 1, posY];
    //        return targetCell;
    //    }
    //    // left
    //    else if (tempCell.mPos.x < posX && gm.mCells[posX - 1, posY].mType == enumCellType.NORMAL && gm.mCells[posX - 1, posY].mBuffType != enumBuffType.EXPLOSION)
    //    {
    //        targetCell = gm.mCells[posX - 1, posY];
    //        return targetCell;
    //    }
    //    // up
    //    else if (tempCell.mPos.y > posY && gm.mCells[posX, posY + 1].mType == enumCellType.NORMAL && gm.mCells[posX, posY + 1].mBuffType != enumBuffType.EXPLOSION)
    //    {
    //        targetCell = gm.mCells[posX, posY + 1];
    //        return targetCell;
    //    }
    //    // down
    //    else if (tempCell.mPos.y < posY && gm.mCells[posX, posY - 1].mType == enumCellType.NORMAL && gm.mCells[posX, posY - 1].mBuffType != enumBuffType.EXPLOSION)
    //    {
    //        targetCell = gm.mCells[posX, posY - 1];
    //        return targetCell;
    //    }

    //    return null;
    //}

    IEnumerator LerpToPos(Vector2 newPos)
    {
        isMooving = true;
        float i = 0;
        float secToWait = 0.01f, lerpIncome = 0.05f;

        Vector2 oldPos = transform.position;
        while ((Vector2)transform.position != newPos)
        {

            transform.position = Vector2.Lerp(oldPos, newPos, i);
            i += lerpIncome;
            yield return new WaitForSeconds(secToWait);

        }

        //StartCoroutine(StartBombTimer());
        //targetCell.BlowCell();
        isMooving = false;
    }
}
