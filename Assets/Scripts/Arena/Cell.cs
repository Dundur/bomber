﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enumCellType
{
    NORMAL,
    BARRIER,
    WALL,

    COUNT
};

public enum enumBuffType
{
    BOMB_STR,
    BOMB_COUNT,

    SPEED,
    HITPOINTS,

    EXPLOSION,

    COUNT
};


public class Cell : MonoBehaviour
{
    public enumCellType mType;
    public enumBuffType mBuffType;
    public int posX;
    public int posY;
    public Vector2 mPos;
    public SpriteRenderer cellSprite;
    public SpriteRenderer sprRend;

    public bool willExplode = false;

    public int gCost;
    public int hCost;
    public int fCost { get { return gCost + hCost; } }
    public Cell parent;

    public TextMesh testText;

    public void Init(enumCellType _type, Vector2 _pos)
    {
        sprRend = GetComponent<SpriteRenderer>();
        cellSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();

        mType = _type;
        posX = (int)_pos.x;
        posY = (int)_pos.y;
        mPos = _pos;

        if (mType == enumCellType.NORMAL)
        {
            sprRend.color = Color.gray;
        }
        else
        if (mType == enumCellType.BARRIER)
        {
            sprRend.color = Color.gray;
            cellSprite.sprite = Resources.Load<Sprite>("Rock");
        }
        else
        if (mType == enumCellType.WALL)
        {
            sprRend.color = Color.black;
        }

        mBuffType = enumBuffType.COUNT;
    }

    public void BlowCell()
    {
        if (mType == enumCellType.WALL)
            return;

        willExplode = false;

        if (mType == enumCellType.BARRIER)
        {
            GameManager.Instance.mBarriers.Remove(this);
            #region Присвоение бафов
            int j = Random.Range(0, 16);
            if (j == 0)
            {
                mBuffType = enumBuffType.BOMB_COUNT;
                cellSprite.sprite = GameManager.Instance.bombCountSprite;
                cellSprite.color = Color.magenta;
                cellSprite.transform.localPosition = Vector3.one / 2;
            }
            else
                if (j == 5)
            {
                mBuffType = enumBuffType.BOMB_STR;
                cellSprite.sprite = GameManager.Instance.bombStrSprite;
                cellSprite.color = Color.cyan;
                cellSprite.transform.localPosition = Vector3.one / 2;
            }
            else
                if (j == 10)
            {
                mBuffType = enumBuffType.SPEED;
                cellSprite.sprite = GameManager.Instance.speedSpite;
                cellSprite.color = Color.yellow;
                cellSprite.transform.localPosition = Vector3.one / 2;
            }
            else
            {
                mBuffType = enumBuffType.COUNT;
                cellSprite.sprite = null;
                cellSprite.transform.localPosition = Vector3.zero;
            }
            #endregion
        }
        else
        {
            mBuffType = enumBuffType.EXPLOSION;
            cellSprite.sprite = null;
        }

        mType = enumCellType.NORMAL;

        StartCoroutine(Recolor(Color.white, Color.gray));
    }

    public void ClearCell()
    {
        mType = enumCellType.NORMAL;
        mBuffType = enumBuffType.COUNT;
        cellSprite.sprite = null;
        StartCoroutine(Recolor(Color.blue, Color.gray));
        
    }

    public void LockCell()
    {
        mType = enumCellType.BARRIER;

        StartCoroutine(Recolor(Color.gray, Color.red));
    }

    IEnumerator Recolor(Color lerpColor, Color newColor)
    {
        float i = 0;
        sprRend.color = lerpColor;
        while (sprRend.color != newColor)
        {
            sprRend.color = Color.Lerp(lerpColor, newColor, i);
            i += 0.01f;
            yield return new WaitForEndOfFrame();
        }

        if (mBuffType == enumBuffType.EXPLOSION)
        {
            mBuffType = enumBuffType.COUNT;
        }
    }

	void Update ()
    {
        if (mType == enumCellType.WALL)
            return;

        if (willExplode)
            sprRend.color = Color.yellow;
        else
            sprRend.color = Color.gray;
	}
}
