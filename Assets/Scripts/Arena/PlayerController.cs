﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MainController
{
    public static PlayerController Instance;

    public Text bombText, dmgText, speedText;
    public Sprite[] HPSprites;
    public Image HPImage;

	override protected void Start ()
    {
        base.Start();
        Instance = this;
    }

	override protected void Update ()
    {
        if (hitPoints < 1)
            return;

        float h = Input.GetAxis("Horizontal") * (moveSpeed / 50);
        float v = Input.GetAxis("Vertical") * (moveSpeed / 50);

        CalculateAngle(h, v);

        if (h > 0)
            if (gm.mCells[Mathf.RoundToInt(transform.position.x + 0.5f), Mathf.RoundToInt(transform.position.y)].mType != enumCellType.NORMAL)
                h = 0;
        if (h < 0)
            if (gm.mCells[Mathf.RoundToInt(transform.position.x - 0.5f), Mathf.RoundToInt(transform.position.y)].mType != enumCellType.NORMAL)
                h = 0;
        if (v > 0)
            if (gm.mCells[Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y + 0.5f)].mType != enumCellType.NORMAL)
                v = 0;
        if (v < 0)
            if (gm.mCells[Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y - 0.5f)].mType != enumCellType.NORMAL)
                v = 0;

        transform.position += new Vector3(h, v, 0);

        base.Update();

        if (Input.GetKeyUp("space") && bombsPlaced < bombCount)
        {
            StartCoroutine(StartBombTimer(posX, posY));
        }
        bombText.text = (bombCount - bombsPlaced).ToString();
    }

    override public void TakeDamage()
    {
        base.TakeDamage();

        HPImage.sprite = HPSprites[hitPoints];
    }
}
