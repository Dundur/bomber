﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{
    protected GameManager gm;
    protected Animator anim;

    protected int hitPoints = 2;
    protected int bombStr = 4;
    protected int bombCount = 5;
    protected float moveSpeed = 2.5f;

    public int posX { get {return Mathf.RoundToInt(transform.position.x); } }
    public int posY { get {return Mathf.RoundToInt(transform.position.y); } }
    public Vector2 mPos() { return new Vector2(posX, posY); }
    public Cell thisCell() { return gm.mCells[posX, posY]; }
    public Cell thisCell(int _x, int _y)
    {
        if (posX + _x > gm.width || posX + _x < 0 || posY + _y > gm.height || posY + _y < 0)
            return gm.mCells[posX, posY];
        return gm.mCells[posX + _x, posY + _y];
    }

    protected bool isBubble = false;

    protected int curAngle = 0;
    protected int bombsPlaced = 0;

    virtual protected void Start ()
    {
        gm = GameManager.Instance;
        anim = GetComponent<Animator>();
    }

	virtual protected void Update ()
    {

        if (gm.mCells[posX, posY].mBuffType == enumBuffType.EXPLOSION)
        {
            TakeDamage();
        }
        else
        if (gm.mCells[posX, posY].mBuffType != enumBuffType.COUNT)
        {
            UseBuff(gm.mCells[posX, posY].mBuffType);
            gm.mCells[posX, posY].ClearCell();
        }
    }

    virtual public void TakeDamage()
    {
        if (isBubble)
            return;

        hitPoints--;

        // dead
        if (hitPoints < 1)
            anim.SetBool("stun", true);

        StartCoroutine(BurnTimer());
        StartCoroutine(BubbleTimer());
    }

    protected void CalculateAngle(float x, float y)
    {
        if (x == 0 && y == 0)
            curAngle = 0;
        else
        if (x > -y && x < y)
            curAngle = 0;
        else
        if (y > -x && y < x)
            curAngle = 1;
        else
        if (x < -y && x > y)
            curAngle = 2;
        else
        if (y < -x && y > x)
            curAngle = 3;

        anim.SetInteger("angle", curAngle);
    }

    protected void UseBuff(enumBuffType _type)
    {
        if (_type == enumBuffType.BOMB_COUNT)
        {
            bombCount++;
        }
        else
            if (_type == enumBuffType.BOMB_STR)
        {
            bombStr++;
        }
        else
            if (_type == enumBuffType.SPEED)
        {
            moveSpeed += 0.25f;
        }
    }

    protected IEnumerator StartBombTimer(int _x, int _y)
    {
        bool up = true, down = true, left = true, right = true;
        bombsPlaced++;
        GameObject newBomb = Instantiate(gm.bombPrefab);
        newBomb.transform.position = new Vector2(_x, _y);

        for (int j = 0; j < 10; j++)
        {
            for (int i = 0; i < bombStr; i++)
            {
                // RIGHT
                if (_x + i < gm.width + 1 && right)
                {
                    if (gm.mCells[_x + i, _y].mType != enumCellType.NORMAL)
                        right = false;
                    gm.mCells[_x + i, _y].willExplode = true;
                }
                // LEFT
                if (_x - i >= 0 && left)
                {
                    if (gm.mCells[_x - i, _y].mType != enumCellType.NORMAL)
                        left = false;
                    gm.mCells[_x - i, _y].willExplode = true;
                }
                // UP
                if (_y + i < gm.height + 1 && up)
                {
                    if (gm.mCells[_x, _y + i].mType != enumCellType.NORMAL)
                        up = false;
                    gm.mCells[_x, _y + i].willExplode = true;
                }
                // DOWN
                if (_y - i >= 0 && down)
                {
                    if (gm.mCells[_x, _y - i].mType != enumCellType.NORMAL)
                        down = false;
                    gm.mCells[_x, _y - i].willExplode = true;
                }
            }
            yield return new WaitForSeconds(0.2f);
        }
       
        up = true; down = true; left = true; right = true;

        for (int i = 0; i < bombStr; i++)
        {
            // RIGHT
            if (_x + i < gm.width + 1 && right)
            {
                if (gm.mCells[_x + i, _y].mType != enumCellType.NORMAL)
                    right = false;
                gm.mCells[_x + i, _y].BlowCell();
            }
            // LEFT
            if (_x - i >= 0 && left)
            {
                if (gm.mCells[_x - i, _y].mType != enumCellType.NORMAL)
                    left = false;
                gm.mCells[_x - i, _y].BlowCell();  
            }
            // UP
            if (_y + i < gm.height + 1 && up)
            {
                if (gm.mCells[_x, _y + i].mType != enumCellType.NORMAL)
                    up = false;
                gm.mCells[_x, _y + i].BlowCell();
            }
            // DOWN
            if (_y - i >= 0 && down)
            {
                if (gm.mCells[_x, _y - i].mType != enumCellType.NORMAL)
                    down = false;
                gm.mCells[_x, _y - i].BlowCell();
            }
        }
        bombsPlaced--;
        Destroy(newBomb, 0.5f);
    }

    protected IEnumerator BubbleTimer()
    {
        isBubble = true;
        anim.SetBool("bubble", true);
        yield return new WaitForSeconds(4.0f);
        anim.SetBool("bubble", false);
        isBubble = false;
    }

    protected IEnumerator BurnTimer()
    {
        anim.SetBool("burn", true);
        yield return new WaitForSeconds(0.8f);
        anim.SetBool("burn", false);
    }
}
